Summary: glite-yaim-core
Name: glite-yaim-core
Version: 5.1.7
Vendor: EGI
Release: 1%dist
License: ASL 2.0
Url: http://svnweb.cern.ch/world/wsvn/curios/glite-yaim-core
Group: System Environment/Admin Tools
Source: %{name}.src.tgz
BuildArch: noarch
Prefix: /opt/glite
Obsoletes: glite-yaim
Provides: glite-yaim
Obsoletes: lcg-yaim
BuildRoot: %{_tmppath}/%{name}-%{version}-build
Packager: CERN
AutoReqProv: yes
Requires: redhat-lsb-core

%description
This package contains the core functionality of YAIM.

%prep

%setup -c

%install
make install prefix=%{buildroot}%{prefix}

%files
%defattr(-,root,root)
%dir %{prefix}/yaim/functions
%dir %{prefix}/yaim/node-info.d
%dir %{prefix}/yaim/log
%{prefix}/yaim/bin/yaim
%{prefix}/yaim/functions/
%{prefix}/share/man/man1/yaim.1
%{prefix}/yaim/defaults/node-info.def
%{prefix}/yaim/defaults/site-info.post
%{prefix}/yaim/defaults/site-info.pre
%{prefix}/yaim/defaults/mapping
%{prefix}/yaim/etc/grid-env-funcs.sh
%{prefix}/yaim/etc/clean-grid-env-funcs.sh
%{prefix}/yaim/etc/grid-env.csh
%{prefix}/yaim/etc/grid-clean-env.csh
%{prefix}/yaim/examples/groups.conf
%{prefix}/yaim/examples/groups.conf.README
%{prefix}/yaim/examples/siteinfo/site-info.def
%{prefix}/yaim/examples/users.conf
%{prefix}/yaim/examples/users.conf.README
%{prefix}/yaim/examples/wn-list.conf
%{prefix}/yaim/examples/wn-list.conf.README
%{prefix}/yaim/examples/edgusers.conf
%{prefix}/yaim/libexec/ClassAd.py
%{prefix}/yaim/libexec/ClassAd.pyc
%{prefix}/yaim/libexec/ClassAd.pyo
%{prefix}/yaim/libexec/ClassAdsHelper.py
%{prefix}/yaim/libexec/ClassAdsHelper.pyc
%{prefix}/yaim/libexec/ClassAdsHelper.pyo
%{prefix}/yaim/libexec/XMLHelper.py
%{prefix}/yaim/libexec/XMLHelper.pyc
%{prefix}/yaim/libexec/XMLHelper.pyo
%{prefix}/yaim/libexec/YAIM2gLiteConvertor.py
%{prefix}/yaim/libexec/YAIM2gLiteConvertor.pyc
%{prefix}/yaim/libexec/YAIM2gLiteConvertor.pyo
%{prefix}/yaim/libexec/configure_node
%{prefix}/yaim/libexec/gLite.def
%{prefix}/yaim/libexec/gLiteContainers.xml
%{prefix}/yaim/libexec/install_node
%{prefix}/yaim/libexec/parse_bash_params
%{prefix}/yaim/libexec/run_function
%{prefix}/yaim/libexec/xmlUtils.py
%{prefix}/yaim/libexec/xmlUtils.pyc
%{prefix}/yaim/libexec/xmlUtils.pyo
%{prefix}/yaim/etc/versions/%{name}


%clean
rm -rf %{buildroot}

