##
#    Class Ad structure
class ClassAd:
    
    def __init__(self):
        self.childNodes = []
     
    def addNode(self,node):
        self.childNodes.append(node)
        node.parent = self
    
    def removeNode(self, node):
        self.childNodes.remove(node)
        
    def findNode(self,name):
        ret = None
        for node in self.childNodes:
            if node.nodeName == name:
                ret = node
                break
        return ret

    def toString(self, indent = ''):
        out = ' [\n'
        for child in self.childNodes:
            out = out + indent + child.toString(indent + '    ')
        out += indent + '];\n'
        return out


##
#    ClassAd document. Contains the whole ClassAd structure
class ClassAdDocument(ClassAd):
    
    def __init__(self,createNodeWhenUpdate = True):
        self.createNodeWhenUpdate = createNodeWhenUpdate
        self.childNodes = []
        
    def toString(self):
        out = '[\n' 
        for classAd in self.childNodes:
            out += classAd.toString('  ')
        out += ']\n'
        return out
    
    def findNodeByPath(self,path):
        ret = None
        paths = path.split('/')
        classAd = self
        for path in paths:
            node = classAd.findNode(path)
            if node == None:
                if self.createNodeWhenUpdate:
                    node = Node(path)
                    classAd.addNode(node)
                    node.setNodeValue(ClassAd())
                else:
                    break
            ret = node
            if isinstance(node.nodeValue, ClassAd):
                classAd = node.nodeValue
            else:
                break
            
        return ret
    
    def updateNodeByPath(self,path,value):
        self.findNodeByPath(path).setNodeValue(value)

    def removeNodeByPath(self,path):
        node = self.findNodeByPath(path)
        node.parent.removeNode(node)
        
##
#    Parameter node
class Node:
    def __init__(self, name):
        self.nodeValue = None
        self.nodeName = name
        self.parent = None
        self.valueType = None

    def isLeaf(self):
        return len(self.childNodes) == 0
        
    def getNodeValue(self):
        return self.nodeValue

    def setNodeValue(self, value):
        #    Value can be:
        #        ClassAds
        #        single value
        #        array value
        self.nodeValue = value
        self.valueType = ''
    

    def toString(self, indent = ''):
        out = self.nodeName + ' = '
        if isinstance(self.nodeValue, ClassAd):
            out += self.nodeValue.toString(indent + '  ')
        elif isinstance(self.nodeValue, list):
          if not len(self.nodeValue) == 1:
            out += '{'
            for value in self.nodeValue:
                out += value
            out += '};\n'
          else:
            out += self.nodeValue[0] + ';\n'
        else:
            out += self.nodeValue + ';\n'
        return out
    
##
#    ClassAd parsing routine
def parse(name):
    file = open(name,'r')
    lines = file.readlines()
    file.close()
    
    doc = ClassAdDocument()
    
    _lines = []
    for line in lines:
        if not line.strip().startswith('//'):
            _lines.append(line)
    
    document = ' '.join(_lines).replace('\n','')
    parseClassAd(doc, document[document.find('[')+1:])
    
    return doc


##
#    Parse classAd value
def parseClassAd(doc, document):
  while not document.strip().find(']') == 0:
    (sectionName, rest) = document.split('=', 1)
    node = Node(sectionName.strip())
    valueChar = rest.strip()[0]
    if valueChar == '[':
        classAds = ClassAd()
        #    classAd
        document = parseClassAd(classAds, rest.strip()[1:].strip())
        node.nodeValue = classAds
    elif valueChar == '{':
        #    array
        node.nodeValue = []
        document = parseArray(node, rest.strip()[1:].strip())
    else:
        #    Normal value
        node.nodeValue = []
        document = parseValue(node, rest.strip())
    doc.addNode(node)
  end = document.find(']')
  return document[end + 2:]

##
#    Parse array value
def parseArray(node, document):
    end = document.find('};')
    params = document[:end].split(',')
    for param in params:
        if not param.strip() == '':
            node.nodeValue.append(param.strip())
    return document[end + 2:].strip()

##
#    Parse single value
def parseValue(node, document):
    end = document.find(';')
    node.nodeValue.append(document[:end])
    return document[end + 1:].strip()
