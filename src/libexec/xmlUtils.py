import sys
import os
import xml.dom.ext
from xml.dom import XML_NAMESPACE, XMLNS_NAMESPACE, DOMException 
from xml.dom.ext import Printer
from xml.dom.minidom import parseString, parse, Node


uiSetStub="""
<set 
    description='Automaticaly generated structure. Please do not change it !! All changes will be overwritten !!!' 
    name=''/>
"""
voSetStub='<ui.VirtualOrganisation   name=""/>'

class GliteUIsetParams:

    def __init__(self, domVO):
        # if domVO==string create dom from a stub
        self.doc = parseString(uiSetStub)
        self.root = self.doc.documentElement
        self.root.setAttribute("name", domVO)
        self.vo = parseString(voSetStub).documentElement
        self.vo.setAttribute("name", domVO)
        self.nsNode = parseString("<ui.NSAddresses/>").documentElement
        self.vo.appendChild(self.nsNode)
        self.root.appendChild(self.vo)
        # if domVO==XMLnode import dom structure
        
    def addNS(self, nsName, lbNames):
        lbNode = parseString("<ui.LBAddresses/>").documentElement
        for lb in lbNames:
            lbNode.appendChild(self.__addLB(lb))
        itemNode = parseString("<item/>").documentElement
        itemNode.setAttribute("value", nsName)
        itemNode.appendChild(lbNode)
        self.nsNode.appendChild(itemNode)
        
    def __addLB(self, lbName):
        valueDoc = parseString("<value/>")
        valueNode = valueDoc.documentElement
        valueNode.appendChild(valueDoc.createTextNode( lbName ))
        return valueNode

    def addParameter(self,name,value):
        param = self.doc.createElement(name)
        param.setAttribute("value",value)
        self.vo.appendChild(param)
 
    def addArrayParameter(self, name, values):
        param = self.doc.createElement(name)
        if values == []:
            values = ['']
        for value in values:
            valueNode = self.doc.createElement("value")
            if value.strip() != '':
                valueNode.appendChild(self.doc.createTextNode( value ))
            param.appendChild(valueNode)
        self.vo.appendChild(param)
 
    def getNode(self):
        return self.root
    



##
#    Following class/function extends the functionality of PrettyPrint 
#
#    As an output it produces text representation of DOM tree with similar
#    formatting as in the gLite configuration files
#
#    Usage
#        from UltraPrint import UltraPrettyPrint
#        from xml.dom.minidom import parse
#        dom = parse('test.xml')
#        UltraPrettyPrint(dom)

class UltraPrintVisitor(Printer.PrintVisitor):
    import xml.dom.ext
    length = 80
    def visitAttr(self, node):
        if node.namespaceURI == XMLNS_NAMESPACE:
            # Skip namespace declarations
            return
        self._write('\n' + self._indent * (self._depth + 1) + node.name)
        value = node.value
        if value or not self._html:
            text = Printer.TranslateCdata(value, self.encoding)
            text, delimiter = Printer.TranslateCdataAttr(text)
            while text.find('  ') > -1:
                text = text.replace('  ',' ')     
            n_text = ''
            while len(text) > self.length:
                s_pos = text.rfind(' ', 0, self.length + 1)
                if s_pos == -1:
                        s_pos = text.find(' ')     
                        if s_pos == -1:
                            s_pos = len(text)
                n_text = n_text + text[0:s_pos] + '\n' + self._indent * (self._depth + 2)
                text = text[s_pos + 1:len(text)]
            text = n_text + text
            self.stream.write("=%s%s%s" % (delimiter, text, delimiter))
        return

##
#    Class implementing the XML output in the form used in the gLite 
#    configuration files
class gliteXMLWriter:
    def __init__(self):
        self.stream = sys.stdout
        
    def setOutputStream(self, stream):
        self.stream = stream 
        
    def writeFile(self, name, root, encoding='UTF-8', indent='  ',
                    preserveElements=None):
        file = open(name,'w')
        self.write(root, file, encoding, indent, preserveElements)
        file.close()
        
    def write(self, root, stream=None, encoding='UTF-8', indent='  ',
                    preserveElements=None):
        stream = stream or self.stream
        if not hasattr(root, "nodeType"):
            return        
        nss_hints = '' #SeekNss(root)
        preserveElements = preserveElements or []
        owner_doc = root.ownerDocument or root
        if hasattr(owner_doc, 'getElementsByName'):
            #We don't want to insert any whitespace into HTML inline elements
            preserveElements = preserveElements + HTML_4_TRANSITIONAL_INLINE
        visitor = UltraPrintVisitor(stream, encoding, indent,
                                       preserveElements, nss_hints)
        Printer.PrintWalker(visitor, root).run()
        stream.write('\n')
        return

##
#    Improved PrettyPrint functionality
#
#    @param root root element of the XML tree to print
#    @param stream stream to output to (should implement the Writer interface
#    @param encoding XML encoding
#    @param indent PrettyPrint indent
#    @param preserveElements PrettyPrint preserveElements
def UltraPrettyPrint(root, stream=sys.stdout, encoding='UTF-8', indent='  ',
                preserveElements=None):
    if not hasattr(root, "nodeType"):
        return
    from xml.dom.ext import Printer
    nss_hints = '' #SeekNss(root)
    preserveElements = preserveElements or []
    owner_doc = root.ownerDocument or root
    if hasattr(owner_doc, 'getElementsByName'):
        #We don't want to insert any whitespace into HTML inline elements
        preserveElements = preserveElements + HTML_4_TRANSITIONAL_INLINE
    visitor = UltraPrintVisitor(stream, encoding, indent,
                                   preserveElements, nss_hints)
    Printer.PrintWalker(visitor, root).run()
    stream.write('\n')
    return
#########################################################################
#        End Ultra Pretty Print
#########################################################################

##
#    method to list the tag names in the NodeList
#    @param nodeList NodeList to work on
#
#    @return list of the tag names
def getNodeNameList(nodeList):
    list = []
    for node in nodeList:
        list.append(node.nodeName)
    return list
    
def getContainerTemplate(containerName):
    containerXml = parse(os.environ['FUNCTIONS_DIR'] + '/../libexec/gLiteContainers.xml')
    containerParentNode = containerXml.getElementsByTagName(containerName)[0]
    for node in containerParentNode.childNodes:
        if node.nodeType == Node.ELEMENT_NODE:
            containerNode = node
            break
    return containerNode