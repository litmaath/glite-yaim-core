Summary: glite-yaim-core
Name: glite-yaim-core
Version: 3.1.0
Vendor: LCG/CERN
Release: 0.1
License:Open Source EGEE License
Group:System/Application
Source: glite-yaim-core.src.tgz
BuildArch: noarch
Prefix:	/opt/glite
BuildRoot: %{_tmppath}/%{name}-%{version}-build
Packager: support-lcg-manual-install@cern.ch

Requires: perl
Requires: python

Provides: lcg-yaim

Obsoletes: lcg-yaim

%description
YAIM is a generic configuration tool for Grid Middleware. 
The word YAIM stands for Yet Another Installation Manager.
But instead of a boring description let a joke be here:

Two system administrator are talking:
 - How you can be so stupid that you are using your dog's name as root password ?
 - Why ? What is the problem with #@_e28X^o  ?

%prep

%setup -c

%build
make install prefix=%{buildroot}%{prefix}

%files
%defattr(-,root,root)
%{prefix}/yaim/functions
%{prefix}/yaim/functions/local
%{prefix}/yaim/libexec
%{prefix}/yaim/scripts
%{prefix}/yaim/examples
%{prefix}/yaim/bin
%{prefix}/etc
%config(noreplace) %{prefix}/yaim/examples/users.conf
%config(noreplace) %{prefix}/yaim/examples/wn-list.conf
%config(noreplace) %{prefix}/yaim/examples/siteinfo/site-info.def
%{prefix}/bin/configure_node
%{prefix}/bin/install_node

%post 
mkdir -p %{prefix}/yaim/log

%clean
rm -rf $RPM_BUILD_ROOT

