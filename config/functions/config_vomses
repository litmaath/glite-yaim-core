##############################################################################
# Copyright (c) Members of the EGEE Collaboration. 2004.
# See http://www.eu-egee.org/partners/ for details on the copyright
# holders.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS
# OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
##############################################################################
#
# NAME :        config_vomses
#
# DESCRIPTION : This function configures the vomses file.
#
# AUTHORS :     Robert.Harakaly@cern.ch
#
# NOTES :
#
# YAIM MODULE:  glite-yaim-core
#
##############################################################################

function config_vomses_check () {

  requires $1 VOS VO__VOMSES
  return $?

}

function config_vomses () {

# clean old vomses path
if [ -d $INSTALL_ROOT/edg/etc/vomses  ]; then
  rm -rf $INSTALL_ROOT/edg/etc/vomses
fi

if [ -f $INSTALL_ROOT/glite/etc/vomses ]; then
  rm -rf $INSTALL_ROOT/glite/etc/vomses
fi
if ( ! echo "${NODE_TYPE_LIST}" | egrep -qi 'TAR' ); then
  if [ ! -d /etc/vomses ]; then
    mkdir -p /etc/vomses
  fi
  chmod 0755 /etc/vomses
else
  if [ ! -d $INSTALL_ROOT/etc/vomses ]; then
    mkdir -p $INSTALL_ROOT/etc/vomses
  fi
  chmod 0755 $INSTALL_ROOT/etc/vomses
fi

for vo in $VOS; do
    #eval vomses='$'VO_`echo $vo | tr '[:lower:]' '[:upper:]'`_VOMSES
    vomses=`get_vo_param ${vo} VOMSES`
    if [ -z "$vomses" ]; then continue; fi

    #
    # beware: the host DN may have embedded spaces!
    # in general a correct input line looks like this:
    # nickname server port DN_part_1 ... DN_part_N alias [GT_version]
    #

    split_quoted_variable $vomses | (
	while read nickname server port rest; do
	    gt=${rest##* }

	    if [ -z "$gt" ]; then
		yaimlog ERROR "VOMSES parameter for VO '$vo' has too few fields"
		exit 1
	    fi

	    # if the "gt" parameter is numeric, chop it off

	    [ -z "${gt//[0-9]}" ] && rest=${rest% *} || gt=

	    alias=${rest##* }
	    dn=${rest% *}

	    if [ -z "$alias" ] || [ -z "$dn" ]; then
		yaimlog ERROR "VOMSES parameter for VO '$vo' has too few fields"
		exit 1
	    fi

	    if [ "x$alias" != "x${alias/\/}" ] || [ "x$dn" = "x${dn/\/}" ]; then
		yaimlog ERROR "VOMSES parameter for VO '$vo' has incorrect DN or alias"
		exit 1
	    fi

            if ( ! echo "${NODE_TYPE_LIST}" | egrep -qi 'TAR' ); then
               filename=/etc/vomses/"${vo}-${server}"
            else
               filename=$INSTALL_ROOT/etc/vomses/"${vo}-${server}"
            fi

	    echo \"$nickname\" \"$server\" \"$port\" \"$dn\" \"$alias\" \
		${gt:+\"$gt\"} > $filename

	    chmod 644 $filename
	done
    ) || return 1
done

return 0

}
