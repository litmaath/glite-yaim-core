from xml.dom.minidom import parse, Node
import os

global dom

def createNodePath(doc, pathElements):
    parent = doc.documentElement
    if not parent.nodeName == pathElements[0]:
        print "Error root element must match"
        os.exit(1)
    for element in pathElements[1:]:
        ok = False
        if element.strip() != '':
            childs = parent.childNodes
            for child in childs:
                if child.nodeName == element:
                    parent = child
                    ok = True
            if not ok:
                elementNode = doc.createElement(element)
                parent.appendChild(elementNode)
                parent = elementNode
    return parent 

def verifyNodePath(doc, nodes, pathElements):
    for node in nodes:
        tmpNode = node
        ok = False
        for i in xrange(len(pathElements)-1,0,-1):
            if tmpNode.parentNode.nodeName == pathElements[i]:
                ok = True
                tmpNode = tmpNode.parentNode
            else:
                ok = False
        if ok:
            return node
    return None
            
def updateTextNode(node, value):
    childs = node.childNodes
    if len(childs) == 0:
        node.appendChild(dom.createTextNode(value))
    else:
        for child in node.childNodes:
            if child.nodeType == Node.TEXT_NODE:
                child.nodeValue = value
                break
    return
    
#    Main program

xmlSource = {}
params = {}
#    Read XMLHelper.conf file
for line in open("XMLHelper.conf").readlines():
    if not line.strip() == '':
        tag,value = line.strip().split('=')
        if tag.startswith('XMLHelper'):
            #    Use tags with XMLHelper.* to configure process itself
            params[tag.split(':',2)[1]] = value.strip()
        else:
            xmlSource[tag] = value

#    Read XML file if exist
print 'loading %s' % params['file']
dom = parse(params['file'])

for xmlSourcePath in xmlSource.keys():
    #    Convert tags from XMLHelper.conf file to XML, update XML !
    path = xmlSourcePath.split(':')
    if path[len(path)-1].startswith('@'):
        tagName=path[len(path)-2]
        attribute = path[len(path)-1]
        path.remove(attribute)
    else:
        tagName=path[len(path)-1]
        attribute = ''
        
    tagNodes = dom.getElementsByTagName(tagName)
    editNode = verifyNodePath(dom, tagNodes, path)
    if editNode == None:
            editNode = createNodePath(dom, path)
    if attribute == '':
            updateTextNode(editNode, xmlSource[xmlSourcePath])
    else:
            editNode.setAttribute(attribute[1:], xmlSource[xmlSourcePath])
    
    
#    Save XML

out = open(params['file'],'w')
out.write(dom.toxml())
out.close
