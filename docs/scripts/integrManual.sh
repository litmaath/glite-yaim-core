#!/bin/sh

#CFG_FILE="functions.def"
CFG_FILE="../../scripts/node-info.def"
VAR_FILE="variables.def"
MODULE_DIR="../modules"
BUILD_DIR="../build"
FUNC_DIR="../../functions"
TABLEDOC="var-table.tex"
DESCDOC="vardesc.tex"

CVSROOT=:pserver:anonymous:@lcgdeploy.cvs.cern.ch:/cvs/lcgdeploy

NODES=`grep "FUNCTIONS=" ${CFG_FILE} | grep -v "^BASE" |awk -F= '{print $1}' | awk -F_ '{for(i=1;i<NF-1;i++) printf $i"_";printf $(NF-1)"\n"}'`

#echo $NODES | awk '{for(i=1;i<=NF;i++) print $i}' > nodelist

echo "... creating general description table"
varTeX.awk -F"+++" -v method="cre_full_desc" -v texout=${DESCDOC} ${VAR_FILE}

echo "... creating ${TABLEDOC} table"
cat << EOF > ${TABLEDOC}.tmp
\label{node-var-table}
\begin{table}[h]   
\centering
\caption{Variables used per node}
\bigskip
{
\footnotesize
\begin{tabular}{|l|p{6.5in}|}
EOF

for node in $NODES ; do 
   echo "... - adding node ${node}"
   VARS=`varTeX.awk -F"+++" -v method="list_node_vars" -v node="${node}" -v funcdir="${FUNC_DIR}" -v nodeinfofile="${CFG_FILE}" ${VAR_FILE}`
   cat << EOF >> ${TABLEDOC}.tmp
\hline
$node & ${VARS} \\\\
EOF
done

cat << EOF >> ${TABLEDOC}.tmp
\hline
\end{tabular}
}
\end{table}
EOF

sed 's/_/\\_/g' ${TABLEDOC}.tmp | sed 's/</$<$/g' | sed 's/>/$>$/g' > ${TABLEDOC}
rm ${TABLEDOC}.tmp

cvs co lcg-docs/LCG2-Manual-Install
cvs co lcg-docs/template
cvs co lcg-docs/Makefile
cp vardesc.tex  ${TABLEDOC} lcg-docs/LCG2-Manual-Install
cd lcg-docs/LCG2-Manual-Install
cvs commit -m "variable description updated" vardesc.tex  ${TABLEDOC}
cd -

#\rm -r lcg-docs/LCG2-Manual-Install
cat << EOF
done.

Now you can use the makefile to compile and publish 
the 'LCG-Manual-Install' document.

run:
------------
cd lcg-docs/LCG2-Manual-Install
make -f ../Makefile
(or, if you have permissions)
make -f ../Makefile publish
------------
#cd lcg-scripts/LCG-Manual-Install 
EOF
