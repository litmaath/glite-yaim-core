#!/bin/sh

CFG_FILE="../../scripts/node-info.def"
BUILD_DIR="../build"
FUNC_DIR="../../functions"

AFSDIR=/afs/cern.ch/project/gd/www/gis/lcg-GCR

NODES=`grep "FUNCTIONS=" ${CFG_FILE} | grep -v "^BASE" |awk -F= '{print $1}' | awk -F_ '{for(i=1;i<NF-1;i++) printf $i"_";printf $(NF-1)"\n"}'`

#NODES=RB


# build all documents
for node in $NODES ;do
   cd ..; make DOC_NAME=$node; cd -
done

#publish pdf
for node in $NODES ;do
   cp ${BUILD_DIR}/${node}.pdf ${AFSDIR}/pdf
done

#publish html
for node in $NODES ;do
   if [ ! -d ${AFSDIR}/html ]; then
      mkdir ${AFSDIR}/html
   fi
   cp -r ${BUILD_DIR}/${node} ${AFSDIR}/html
done

# create web page and hyperlinks
***********************************************************
echo "copy the following links in the ${AFSDIR}/index.html"

cat << EOF > ${AFSDIR}/index.html
<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=?keywords? content=?LCG Generic Installation Guides>
<meta name="DC.subject" content="LCG Configuration Reference">
<meta name="DC.creator" content="Retico, Antonio, Antonio.Retico@cern.ch">
<meta name="DC.title" content="LCG Configuration Reference">
<meta name="DC.language" scheme="ISO639" content="en">
<meta name="DC.publisher" content="CERN">
<meta name="category" content="MI">
<title>LCG Configuration Reference</title>
</head>
<body lang=EN-US link=blue vlink=purple style='tab-interval:.5in' bgcolor="#FFFFFF" text="#000000">
<h1 style='text-align:center'><font size=7 font-family=Comic Sans MS color=navy>LCG</font> <font color=black><font color=red>C</font>onfiguration <font color=green>R</font>eference </font><br>
</h1>
<p><br>
<p style='text-align:center'><strong>Welcome in the LCG Configuration Reference Site.</strong></p><br>
<p><strong> In this site the complete reference for the configuration actions 
    needed for the manual cofiguration of the LCG nodes is published. <br>
<strong>WARNING: The use of this documents in order to actually set up the 
    nodes manually is not recommended, because the manual procedure is slow 
    and error prone. </strong><br>
    If you are going for a manual installation of a default LCG site you should
    refer to   
    <p style='text-align:center'> <a href="http://www.cern.ch/grid-deployment/documentation/LCG2-Manual-Install/"><em> 
    Generic Installation Guide</em></a></p><br><br>
    Configuration References are currently available for the following nodes:</strong></p>
<hr>
<UL>
EOF

for node in $NODES; do
   echo "<LI><strong>"${node}" </strong>: <a href=\"pdf/"${node}".pdf\"> pdf </a>, <a href=\"html/"${node}"/"${node}".html\"> html </a></LI>" >> ${AFSDIR}/index.html
done

cat << EOF >> ${AFSDIR}/index.html
</UL>
<hr>
<p><strong><em>Contacts:</em></strong></p>
<p><a href="mailto:support-lcg-manual-install@cern.ch"><em><font size="-1">support-lcg-manual-install@cern.ch</a></em></font></p>
<hr>
<o:p></o:p>
EOF
