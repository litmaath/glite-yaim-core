import sys, os
sys.path.append("%s/yaim/libexec" % os.environ["GLITE_LOCATION"])
from ClassAd import ClassAd, Node, parse, ClassAdDocument

classAdsSource = {}
params = {}
#    Read XMLHelper.conf file
for line in open("ClassAdsHelper.conf").readlines():
    if not line.strip() == '':
        tag,value = line.strip().split('=', 1)
        if tag.startswith('ClassAdsHelper'):
            #    Use tags with XMLHelper.* to configure process itself
            params[tag.split(':',2)[1]] = value.strip()
        else:
            classAdsSource[tag] = value
            



classAd = ClassAdDocument()
for tag in classAdsSource.keys():
    classAd.updateNodeByPath(tag, classAdsSource[tag])
out = open(params['file'],'w')
out.write(classAd.toString())
out.close()
