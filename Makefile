prefix=/opt/glite
package=glite-yaim-core
name=$Name: emi-yaim-core_R_5_1_7_1 $
tag:=$(shell echo $(name) | sed 's/^[^:]*: //' )
version:=$(shell echo "$(tag)" | sed 's/^.*R_//' | sed 's/_/\./g')
release:=$(shell echo "$(version)" | sed 's/.*\.//')
version:=$(shell echo "$(version)" | sed 's/\(.*\)\.[0-9]*/\1/')

.PHONY: configure install clean rpm

all: configure

install: 
	@echo installing ...
	@mkdir -p $(prefix)/yaim/functions/local
	@mkdir -p $(prefix)/yaim/functions/utils
	@mkdir -p $(prefix)/yaim/functions/extensions
	@mkdir -p $(prefix)/yaim/node-info.d
	@mkdir -p $(prefix)/yaim/libexec
	@mkdir -p $(prefix)/yaim/examples
	@mkdir -p $(prefix)/yaim/examples/siteinfo
	@mkdir -p $(prefix)/yaim/defaults
	@mkdir -p $(prefix)/yaim/bin
	@mkdir -p $(prefix)/yaim/etc
	@mkdir -p $(prefix)/yaim/etc/versions
	@mkdir -p $(prefix)/yaim/log
	@mkdir -p $(prefix)/bin
	@mkdir -p ${prefix}/share/man/man1
	@echo "$(package) $(version)-$(release)" > $(prefix)/yaim/etc/versions/$(package)

	@install config/functions/utils/central_certs $(prefix)/yaim/functions/utils/
	@install config/functions/utils/check_users_conf_format $(prefix)/yaim/functions/utils/
	@install config/functions/utils/config_file $(prefix)/yaim/functions/utils/
	@install config/functions/utils/convert_fqan $(prefix)/yaim/functions/utils/
	@install config/functions/utils/cron_job $(prefix)/yaim/functions/utils/
	@install config/functions/utils/detect_platform $(prefix)/yaim/functions/utils/
	@install config/functions/utils/detect_tomcat $(prefix)/yaim/functions/utils/
	@install config/functions/utils/gridenv $(prefix)/yaim/functions/utils/
	@install config/functions/utils/requires $(prefix)/yaim/functions/utils/
	@install config/functions/utils/run $(prefix)/yaim/functions/utils/
	@install config/functions/utils/set_mysql_passwd $(prefix)/yaim/functions/utils/
	@install config/functions/utils/split_quoted_variable $(prefix)/yaim/functions/utils/
	@install config/functions/utils/start_mysql $(prefix)/yaim/functions/utils/
	@install config/functions/utils/users_* $(prefix)/yaim/functions/utils/
	@install config/functions/utils/vo_param $(prefix)/yaim/functions/utils/
	@install config/functions/utils/yaimlog $(prefix)/yaim/functions/utils/

	@install -m 0644 config/functions/config* $(prefix)/yaim/functions
	@install -m 0644 config/functions/create* $(prefix)/yaim/functions
	@install -m 0644 examples/groups.conf $(prefix)/yaim/examples
	@install -m 0644 examples/users.conf.README $(prefix)/yaim/examples
	@install -m 0644 examples/groups.conf.README $(prefix)/yaim/examples
	@install -m 0644 examples/users.conf $(prefix)/yaim/examples
	@install -m 0644 examples/wn-list.conf $(prefix)/yaim/examples
	@install -m 0644 examples/wn-list.conf.README $(prefix)/yaim/examples
	@install -m 0644 examples/edgusers.conf $(prefix)/yaim/examples
	@install -m 0644 examples/siteinfo/site-info.def $(prefix)/yaim/examples/siteinfo/
	@install -m 0644 src/man/yaim.1 ${prefix}/share/man/man1/

	@install -m 0755 src/libexec/*.py $(prefix)/yaim/libexec
	@install -m 0755 src/libexec/gLiteContainers.xml $(prefix)/yaim/libexec
	@install -m 0755 src/libexec/gLite.def $(prefix)/yaim/libexec
	@install -m 0755 src/libexec/configure_node $(prefix)/yaim/libexec
	@install -m 0755 src/libexec/install_node $(prefix)/yaim/libexec
	@install -m 0755 src/libexec/parse_bash_params $(prefix)/yaim/libexec
	@install -m 0755 src/libexec/run_function $(prefix)/yaim/libexec
	@install -m 0644 src/defaults/node-info.def $(prefix)/yaim/defaults
	@install -m 0644 src/defaults/site-info.post $(prefix)/yaim/defaults
	@install -m 0644 src/defaults/site-info.pre $(prefix)/yaim/defaults
	@install -m 0644 src/etc/grid* $(prefix)/yaim/etc
	@install -m 0644 src/defaults/mapping $(prefix)/yaim/defaults
	@install -m 0644 src/etc/clean* $(prefix)/yaim/etc
	@install -m 0755 src/bin/yaim $(prefix)/yaim/bin


clean::
	rm -f *~ test/*~ etc/*~ doc/*~ src/*~  
	rm -rf rpmbuild 

rpm:
	@mkdir -p  RPMS
	@mkdir -p  rpmbuild/RPMS/noarch
	@mkdir -p  rpmbuild/SRPMS/
	@mkdir -p  rpmbuild/SPECS/
	@mkdir -p  rpmbuild/SOURCES/
	@mkdir -p  rpmbuild/BUILD/

ifneq ("$(tag)","ame:")
	@sed -i 's/^Version:.*/Version: $(version)/' $(package).spec
	@sed -i 's/^Release:.*/Release: $(release)%dist/' $(package).spec
endif
	@tar --gzip --exclude='*CVS*' --exclude='rpmbuild*' --exclude='RPMS' -cf rpmbuild/SOURCES/${package}.src.tgz *
	rpmbuild --define 'topdir %(pwd)/rpmbuild' --define '_topdir %{topdir}' -ba ${package}.spec
	cp rpmbuild/RPMS/noarch/*.rpm rpmbuild/SRPMS/*.rpm RPMS/.


