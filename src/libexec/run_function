#!/bin/bash

function run_function () {

hostname=`hostname -f | tr '[:upper:]' '[:lower:]'`


# Populate NODE_TYPE_LIST so the functions know what node types we are

if [ -z "$NODE_TYPE_LIST" ]; then 

unset NODE_TYPE_LIST

for i in CE WMS RB BDII PX MON LFC VOBOX CLASSIC DPM DCACHE TORQUE FTS STORM; do
    case $i in
    CE) hostvar=$CE_HOST; nodetype=CE;;
    WMS) hostvar=$WMS_HOST; nodetype=WMSLB;;
    RB) hostvar=$RB_HOST; nodetype=RB;;
    BDII) hostvar=$BDII_HOST; nodetype=BDII;;
    PX) hostvar=$PX_HOST; nodetype=PX;;
    MON) hostvar=$MON_HOST; nodetype=MON;;
    LFC) hostvar=$LFC_HOST; nodetype=LFC;;
    VOBOX) hostvar=$VOBOX_HOST; nodetype=VOBOX;;
    CLASSIC) hostvar=$CLASSIC_HOST; nodetype=SE_classic;;
    DPM) hostvar=$DPM_HOST; nodetype=SE_dpm_mysql;;
    DCACHE) hostvar=$DCACHE_ADMIN; nodetype=SE_dcache;;
    TORQUE) hostvar=$BATCH_SERVER; nodetype=CE_torque;;
    FTS) hostvar=$FTS_HOST; nodetype=FTS;;
    STORM) hostvar=$STORM_BACKEND_HOST; nodetype=SE_storm_backend;;
    esac
    
    if [ "x$hostname" == "x$hostvar" ]; then
	NODE_TYPE_LIST="${NODE_TYPE_LIST} $nodetype"
    fi
done

unset hostvar nodetype

fi

if ( echo $NODE_TYPE_LIST | grep -qw CE_torque ); then
    NODE_TYPE_LIST=${NODE_TYPE_LIST/CE / }
fi

if [ -r "${WN_LIST}" ]; then
    if ( grep $hostname ${WN_LIST} > /dev/null ); then
	NODE_TYPE_LIST="${NODE_TYPE_LIST} WN"
    fi
fi

if ( echo $DPM_FILESYSTEMS | egrep -q $hostname ); then
	NODE_TYPE_LIST="${NODE_TYPE_LIST} SE_dpm_disk"
fi

NODE_TYPE_LIST_TMP=`rpm -qa | grep storm_ | perl -pi -e 's/(.+)-.+-.+/\1/'`
for i in `echo $NODE_TYPE_LIST_TMP | tr ' ' '\n' `; do
    node=`echo ${i} | sed -e 's/^lcg-//' -e 's/^glite-//' -e 's/^egee-//' -e 's/^emi-//'`
    NODE_TYPE_LIST="${NODE_TYPE_LIST} $node"
done

if [ -z "$NODE_TYPE_LIST" ]; then
    NODE_TYPE_LIST="UI"
fi

yaimlog INFO "Assuming the node types: $NODE_TYPE_LIST"
yaimlog INFO "Using hostname: $hostname"

# Source node-info.d for all configured services

for node in ${NODE_TYPE_LIST} ; do

    # Build the node-info.d file name to be sourced
      if ( echo ${node} | egrep -q 'creamCE' ) ; then
         node_info_file=${mythisdir}/../node-info.d/glite-creamce
      fi
      if ( echo ${node} | egrep -q 'MPI_CE' ) ; then
        node_info_file=${mythisdir}/../node-info.d/glite-mpi_ce
      else # glite config targets
        if [ -f "${mythisdir}/../node-info.d/glite-`echo $node | tr '[:upper:]' '[:lower:]'`" ] ; then
           node_info_file=${mythisdir}/../node-info.d/glite-`echo $node | tr '[:upper:]' '[:lower:]'`
        else  # _ cases
           if [ -f "${mythisdir}/../node-info.d/glite-`echo $node | tr '[:upper:]' '[:lower:]' | cut -d'_' -f1`" ] ; then
              node_info_file=${mythisdir}/../node-info.d/glite-`echo $node | tr '[:upper:]' '[:lower:]' | cut -d'_' -f1`
           else # egee config targets
              if [ -f "${mythisdir}/../node-info.d/egee-`echo $node | tr '[:upper:]' '[:lower:]'`" ] ; then
                 node_info_file=${mythisdir}/../node-info.d/egee-`echo $node | tr '[:upper:]' '[:lower:]'`
              else # emi config targets
                 if [ -f "${mythisdir}/../node-info.d/emi-`echo $node | tr '[:upper:]' '[:lower:]'`" ] ; then
                    node_info_file=${mythisdir}/../node-info.d/emi-`echo $node | tr '[:upper:]' '[:lower:]'`
                 else # _ cases
                   if [ -f "${mythisdir}/../node-info.d/emi-`echo $node | tr '[:upper:]' '[:lower:]' | cut -d'_' -f1`" ] ; then
                      node_info_file=${mythisdir}/../node-info.d/emi-`echo $node | tr '[:upper:]' '[:lower:]' | cut -d'_' -f1`
                   else # No glite, egee or emi prefix
                      node_info_file=${mythisdir}/../node-info.d/`echo $node | tr '[:upper:]' '[:lower:]'`
                   fi # everything
                 fi # _ cases
              fi # emi config targets
            fi # egee config targets
         fi # glite _ config targets
      fi # glite config targets

    # Source node-info.d/${node}
    if [ -f "$node_info_file" ]; then
        yaimlog DEBUG "Sourcing node definition file: $node_info_file"
        source $node_info_file
    else
        eval ${node}_FUNCTIONS='"'$(
            if [ -f "${mythisdir}/../defaults/node-info.def" ] ; then
                source ${mythisdir}/../defaults/node-info.def
                eval echo '$'`echo ${node}_FUNCTIONS`
            fi
            )'"'
        yaimlog DEBUG "Sourcing node definition file: ${mythisdir}/../defaults/node-info.def"
    fi
done

# Check if profile supports the function
for profile in ${NODE_TYPE_LIST}; do
    # BDII site
    if ( echo ${profile} | egrep -q 'BDII' ); then
      if [ $HOSTNAME = $BDII_HOST ]; then
        profile_functions=`eval echo '$'BDII_top_FUNCTIONS`
      else
        profile_functions=`eval echo '$'BDII_site_FUNCTIONS`
      fi
    # other profiles
    else
      profile_functions=`eval echo '$'${profile}_FUNCTIONS`
    fi

    supp_functions="$profile_functions $supp_functions"
done
if [ -z "$supp_functions" ]; then
    yaimlog ERROR "Profile functions not defined. Exiting."
    exit 1
fi

yaimlog DEBUG "Initializing VO paramters."
vo_param_init $config_dir

for FUNCTION in "$@"; do
   # Check if profile supports the funcion
   if [ ! "`echo $supp_functions | egrep $FUNCTION`" ]; then
     yaimlog WARNING "$FUNCTION function is not supported by ${NODE_TYPE_LIST}!"
     yaimlog WARNING "Anyway you can force this operation."
     yaimlog WARNING "Would you really run $FUNCTION on ${NODE_TYPE_LIST}? [y/]: "
     read -s -n1 INPUT
     if [ ! "$INPUT" = "y" ]; then
       yaimlog WARNING "Exiting."
       exit 0
     fi
   fi

   run check ${FUNCTION}_check 2>>/dev/null
   myret=$?
   if [ ${myret} -eq 1 ] ; then
       yaimlog ERROR "Error during running check functions."
       yaimlog ERROR "Please define your missing variables ! Exiting."
       exit 1;
   fi
   run ${FUNCTION}_setenv 2>> /dev/null
   run ${FUNCTION}
   myret=$?
   if [ ${myret} -ne 0 ]; then
       echo_failure;
       yaimlog ERROR "Error during the configuration.Exiting.";
       exit 1;
   fi
done

yaimlog DEBUG "Closing VO paramters."
vo_param_close

}
