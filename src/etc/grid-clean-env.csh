#
# This is the file which cleans the grid environment for tcsh shell. Have a look to the grid-clean-env.sh file !
#
#

set mycshtmpfile=`mktemp /tmp/gridenv.XXXXXX`
bash -c "export ISCSHELL=yes ; source GRID_ENV_LOCATION" >> $mycshtmpfile 
source $mycshtmpfile 
rm -f $mycshtmpfile

