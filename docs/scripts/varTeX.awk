#!/usr/bin/awk -f

# calls:
# varTeX.awk -F"+++" -v method="cre_full_desc" -v texout="vardesc.tex" variables.def
# varTeX.awk -F"+++" -v method="cre_func_desc" -v funct="../../functions/config_gip" variables.def
# varTeX.awk -F"+++" -v method="cre_node_desc" -v node="CE" -v funcdir="../../functions" -v nodeinfofile="../../scripts/node-info.def" variables.def
# varTeX.awk -F"+++" -v method="list_node_vars" -v node="CE" -v funcdir="../../functions" -v nodeinfofile="../../scripts/node-info.def" variables.def

BEGIN {varNO=0
         }
/^[ \t]*#/ { next }  # Skip comment lines
/^$/       { next }  #    and blank lines                   
{id=$1
   HT_name[id]=$2
   HT_description[id]=$3
   HT_parent[id]=$4
   # trim possible trailing spaces: they disturb the test
   gsub(/[ ]+$/, "",HT_parent[id])
   varNO = varNO + 1
   v_id[varNO]=$1
}
  
END {
  if ( method ) {
    if ( method == "cre_full_desc" ){
      retcode = f_creFullDesc(); 
    }
    else if (method == "cre_func_desc"){
      retcode = f_creFuncDesc();
    }
    else if (method == "cre_node_desc"){
      retcode = f_creNodeDesc();
    }
    else if (method == "list_node_vars"){
      retcode = f_listNodeVars();
    }
    else {
      print "undefined method ("method")";
      exit 1;
    }
    if ( retcode != 0 ){
      print "Error running method "method": Exit";
      exit 1;
    }
  }
} 

############################################################
#
# f_existPName( Pathname )
#
#       Controlla se esiste il pathname (directory o file) specificato.
#       Ritorna lo stdout prodotto dal relativo comando "ls".
#
# Es.:
# ExistFFile = f_ExistPName( Pathname )
# if ( ExistFFile !=1 ) {
#       # Se non esiste il file, lo crea.
#       system( "touch " Pathname )
# }
#
############################################################
function f_existPName( pathname ){
  Cmd = "ls " pathname " 2> /dev/null ";
  Cmd | getline ExistPName;
  close( Cmd );
  return ( ExistPName );        
}

############################################################
#
# f_Basename( Pathname )
#
#       Estrae dalla stringa ricevuta (Pathname), la sottostringa ( FileName )
#       che segue l'eventuale ultimo carattere "/" (slash).
#
# FileName = f_Basename( Pathname )
#
############################################################
function f_baseName( Pathname )
{
        while ( l_PosChar = index( Pathname, "/" ) ) { Pathname = substr( Pathname, l_PosChar + 1 ) }   
        return ( l_FileName = Pathname )
}

############################################################
#
#       Create a description latex file using the  
#       variables in the input hashes.
#
############################################################

function f_creStructDesc( v_pID, HT_pname, HT_pdescription, HT_pparent, texFileOut){
  
  outfile=texFileOut".tmp";
  texoutfile=texFileOut;
 
  print "\\begin{description}" > outfile;

  isThereChild = "0";
  n = asort(HT_pname, sHT_pname);
  for (i = 1; i <= n; i++) {
	for (j in v_pID) {
		id = v_pID[j];
		if (HT_pname[id] == sHT_pname[i])
			break;
	}
        if( HT_pparent[id] == "0"){
      		name=HT_pname[id];
     	 	description=HT_pdescription[id];
      		print "\\item ["name" :] "description"." >> outfile;
    	} else 
		isThereChild = "1";
  }
  
  print "\\item" >> outfile; # empty item to allowd pdflatex compilation if the list is empty
  print "\\end{description}" >> outfile;
  
  # if existing, handle second level variables
  if (isThereChild == "1"){

    # create partial lists
    system ("rm -f  *.itms");
    for (i = 1; i <= n; i++){
      for (j in v_pID) {
        id=v_pID[j];
        if (HT_pname[id] == sHT_pname[i])
          break;
      }
      if( HT_pparent[id] != "0"){
        parentID=HT_pparent[id];
        name=HT_pname[id];
        description=HT_pdescription[id];
        parentName=HT_name[parentID];
        print "\\item ["name" :] "description"." >> parentName".itms";
      }
    }
  
    # get the list of parents with a ls
    str_parentNames = sprintf("ls *.itms 2>/dev/null | awk -F. '{print $1}'");
    ind=0;
    while ((str_parentNames | getline v_parentName[ind]) > 0){
      ind++;
    }
    close(str_parentNames);
  
    # insert the child lists in the appropriate place
    for (i=0;i<ind;i++ ){
      system ("rm -f top.tex bottom.tex");
      parentName=v_parentName[i];
      cmd_split="awk 'BEGIN {fout=\"top.tex\"} \
               /"parentName"/ {if (fout) \
                                   {close(fout)};\
                               fout=\"bottom.tex\"};\
                               {print >> fout};\
                        END {close(fout)}' "outfile;  
      cmd_insert="head -1 bottom.tex >> top.tex ; \
           printf \"For each item listed in the "parentName" variable you need to create a set of new variables as follows:\\n\\\\\\begin{description}\\n\" >> top.tex ; \
           cat "parentName".itms >> top.tex 2>/dev/null; \
           echo \"\\end{description}\" >> top.tex ; \
           cat bottom.tex | awk 'NR>1 {print $0}' >> top.tex ;\
           mv top.tex "outfile;
      system(cmd_split";"cmd_insert"; rm -f "parentName".itms bottom.tex")
        }
  }
  cmd_textransl="sed 's/_/\\\\_/g' "outfile"| sed 's/</$<$/g'| sed 's/>/$>$/g' > "texoutfile;
  system(cmd_textransl";rm "outfile);
  return 0;
}

############################################################
#
#       Print the names of variables from the hash 
#
############################################################

function f_printNodeVars( v_pID, HT_pname, HT_pdescription, HT_pparent){
  
#  outfile=texFileOut".tmp";
#  texoutfile=texFileOut;
 
#  print "\\begin{description}" > outfile;

  # find out who has children
    # init hashes 
  for (i in v_pID ){
    id=v_pID[i];
    name=HT_pname[id];
    HT_hasChildren[name] = "N";
    HT_alreadyPrinted[name] = "N";
  }
    # insert true value for actual parents
  isThereChild = "0";
  for (i in v_pID ){
    id=v_pID[i];
    if( HT_pparent[id] != "0"){
      parent_id=HT_pparent[id];
      parent_name=HT_pname[parent_id];
      HT_hasChildren[parent_name] = "Y";
      isThereChild = "1";
    }
  }
  
  # print
  len = asort(HT_pname, sHT_pname);
  for (i = 1; i <= len; i++) {
	for (j in v_pID) {
		id = v_pID[j];
		if (HT_pname[id] == sHT_pname[i])
			break;
	}
      name=HT_pname[id];
      parent_id=HT_pparent[id];
      has_children=HT_hasChildren[name];
      if (isThereChild == "0") {
         printf name", ";
         HT_alreadyPrinted[name] = "Y"
      }
      else {
         if (parent_id == "0"){
            if (HT_alreadyPrinted[name] == "N"){
               printf name", ";
               HT_alreadyPrinted[name] = "Y";
            }
            if (has_children == "Y"){
               for (k = 1; k <= len; k++) {
                  for (n in v_pID ){
                     chi_id=v_pID[n];
                     if (HT_pname[chi_id] == sHT_pname[k])
                        break;
                  }
                  chi_name=HT_pname[chi_id];
                  chi_par_id=HT_pparent[chi_id];
                  if (chi_par_id == id){
                     if (HT_alreadyPrinted[chi_name] == "N"){
                        printf chi_name", ";
                        HT_alreadyPrinted[chi_name] = "Y"
                     }
                  }
               }
            }
         }
      }      
   }
   printf "\n";
   return 0;
}


############################################################
#
#       Create the description latex file with all the 
#       variables defined in the configuration file.
#
############################################################
function f_creFullDesc(){
  if ( texout ){
    retcode = f_creStructDesc( v_id, HT_name, HT_description, HT_parent, texout);  
  }
  else {
    print "Error: method " method " needs the variable 'texout' to be specified";
    retcode = -1;
  }
return retcode;

}

############################################################
#
#       Create the description latex file with the variables
#       used by a single function
#
############################################################
function f_creFuncDesc(){

  if ( ! funct) {
    print "Error: method " method " needs the variable 'funct' to be specified";
    retcode = -1;
  }
  else{
    funcFile = f_existPName(funct);
    if ( funcFile != funct ){
      print "Error: cannot find function file " funct;
      retcode = -1;
    }
    else{
      funcName = f_baseName(funcFile);
      outfile=funcName"_vars.tex";
      
      # create hash of used PARENT variables
      ind = 0;
      for (i in v_id ){
          id = v_id[i];
          pattern = HT_name[id];
          if (HT_parent[id] != "0")
	    gsub("<.*>", ".*", pattern) # match children
          str_grepVar = "grep '\\<"pattern"\\>' "funcFile" | grep -v ^# | head -1";   
          while ((str_grepVar | getline v_grepResult[ind]) > 0){
            v_usedId[ind]=id;
            HT_usedName[id] = HT_name[id];
            HT_usedDescription[id] = HT_description[id];
            HT_usedParent[id] = HT_parent[id];
            ind++;
          }
	  close(str_grepVar);
      }

      # create the description tex files
      retcode =  f_creStructDesc(v_usedId, HT_usedName, HT_usedDescription, HT_usedParent, outfile);
    } 
  }
  return retcode;
}

############################################################
#
#       Chech the input for the methods dealing with node
#       description. E.G. 'cre_node_desc', 'list_node_variables'
#       This is an ugly function. It has been excerpted by the 
#       original location (inside the f_creNodeDesc() code), in 
#       order for the code to be re-used by f_listNodeVars()
#
############################################################
function f_nodeChkInput(){
   if ( ! node ) {
      print "Error: method " method " needs the variable 'node' to be specified";
      retcode = -1;
   }
   else if ( ! nodeinfofile ){ 
      print "Error: method " method " needs the variable 'nodeinfofile' to be specified";
      retcode = -1;
   }
   else if ( ! funcdir ){ 
      print "Error: method " method " needs the variable 'funcdir' to be specified";
      retcode = -1;
   }
   else {
      infoFile = f_existPName(nodeinfofile);
      if ( infoFile != nodeinfofile ){
         print "Error: cannot find node info file " nodeinfofile;
         retcode = -1;
      }
   }
   return retcode;
}

############################################################
#
#       Populate the description hash tables with the 
#       variables used by a node.
#       this is an ugly function that works mostly as
#       a side effect. It has been excerpted by the original
#       location (inside the f_creNodeDesc() code), in order
#       for the code to be re-used by f_listNodeVars()
#
############################################################
function f_nodePopulateHash(){
   
   # get the list of functions for that node
   str_funcList = sprintf(". " nodeinfofile"; echo $"node"_FUNCTIONS");
   str_funcList | getline functList;
   #while ((str_funcList | getline functList) > 0){
   #  print functList;
   #}
   close(str_funcList);
   funcno = split(functList, v_function, " ");
   for ( count=1; count<=funcno; count++ ){
      funcName = v_function[count];
      testFuncFile =  funcdir"/"funcName
      funcFile = f_existPName(testFuncFile)
      if ( funcFile != testFuncFile){
         print "Error: cannot find function file " funcdir"/"funct;
         retcode = -1;                 
      }
      else {
         # create hash of used PARENT variables
         ind = 0;
         for (i in v_id ){
              id=v_id[i];
              pattern = HT_name[id];
              if (HT_parent[id] != "0")
                 gsub("<.*>", ".*", pattern);
              str_grepVar = "grep '\\<"pattern"\\>' "funcFile" | grep -v ^# | head -1";   
              while ((str_grepVar | getline v_grepResult[ind]) > 0){
                  v_usedId[ind]=id;
                  HT_used[id]="Y";
                  HT_usedName[id] = HT_name[id];
                  #print "PAR:"HT_name[id]"  ID="id;
                  HT_usedDescription[id] = HT_description[id];
                  HT_usedParent[id] = HT_parent[id];
                  ind++;
               }
               close(str_grepVar);   
         }
      }
   }
   # remove duplicates from array of used variables
   i=0;
   for ( i in v_id ){
      id=v_id[i];
      if (HT_used[id] == "Y"){
         v_usedIdnodup[i] = id;
         i++;
      }
   }
   return retcode;
}


############################################################
#
#       Create the description latex file with all the 
#       variables used by a node.
#
############################################################
function f_creNodeDesc(){

   retcode = f_nodeChkInput()
   if  ( retcode != 0 ){
      print "Error in input parameters of method "method": Exit";
      retcode = -1;
   }
   else {
      outfile=node"_vars.tex";
         
      # populate the arrays
      retcode = f_nodePopulateHash();
                    
      # create the description tex file
      retcode =  f_creStructDesc(v_usedIdnodup, HT_usedName, HT_usedDescription, HT_usedParent, outfile);
   }
   return retcode;
}

############################################################
#
#       Create a table with the list of variables used by a 
#      given node  output =
#       <NODE-NAME> <VAR1>,<VAR2>,<VAR3>...
#
############################################################
function f_listNodeVars(){

   retcode = f_nodeChkInput()
   if  ( retcode != 0 ){
      print "Error in input parameters of method "method": Exit";
      retcode = -1;
   }
   else {
         
      # populate the arrays
      retcode = f_nodePopulateHash();
                    
      # create the description tex file
      retcode =  f_printNodeVars(v_usedIdnodup, HT_usedName, HT_usedDescription, HT_usedParent);
   }
   return retcode;
}
