################################################################################
#
# Copyright (c) Members of the EGEE Collaboration. 2004.
# See http://eu-egee.org/partners/ for details on the copyright holders.
# For license conditions see the license file or http://eu-egee.org/license.html
#
################################################################################

import os
import re
import sys
import getopt
import shutil
import xmlUtils
from xml.dom.minidom import Node
from xml.dom.minidom import parse, parseString
from xmlUtils import gliteXMLWriter
from xmlUtils import UltraPrettyPrint
from xmlUtils import GliteUIsetParams

global vo, map, defaults, yaim_file, environ

def isGliteScalar( node ):
    return not node.hasChildNodes() 

def isArray( value ):
    return value.count( '"' ) > 0 or value.count( "'" ) > 0

def updateArrayParameter( node, value, yaimParam, description = '' ):
    if not value.strip() == '':
      externQuote = value.strip()[0]
      if externQuote in ('"',"'"):
        quoteCount =  value.count( externQuote )
        splitChar = externQuote
        if quoteCount % 2 > 0:
            print "possible problems: odd number of quotes found"
        elif quoteCount == 2:
            value = value.strip( externQuote )
            splitChar = ' '
      else:
        splitChar = ' '

    #value = value.strip( externQuote )
    #    parse array items from value
      itemValues = value.split( splitChar )
    else:
      itemValues = []
    #    cleanup of empty entries
    
    if isGliteScalar( node ):
        updateParamNode( node, itemValues[0], yaimParam, description )
    else:
        for i in range( len( itemValues ) - 1, -1, -1 ):
            if stripQuotes( itemValues[i] ) == '':
                itemValues.remove( itemValues[i] )
            
        #    remove all existing values from array
        for child in range( len( node.childNodes ) - 1, -1, -1 ):
            node.removeChild( node.childNodes[child] )
            
        if description == '':
            description = 'Parameter defined in %s by YAIM parameter %s'\
                       % ( yaim_file, yaimParam )    
                       
        node.setAttribute( 'description', description )
        
        #    Add special treatement for SE_LIST
        itemValues1 = []
        if yaimParam == 'SE_LIST':
#            for item in itemValues:
#                itemValues1.append( "'.' %s /tmp" % item )
            for item in xrange( len( itemValues ) ):
                try:
                    itemValues1.append( "'.' %s %s" % ( itemValues[item], \
                        stripQuotes( replaceParameter( environ['CLASSIC_STORAGE_DIR'] )\
                        .split( ' ' )[item] ) ) )
                except:
                    itemValues1.append( "'.' %s /tmp" % itemValues[item] )
            itemValues = itemValues1
                           
        #    add new items to array
        if len( itemValues ) == 0:
            itemValues = ['']
        for value in itemValues:
            newDoc = parseString( '<value/>' )
            newItem = newDoc.documentElement
            if not value.strip() == '':
                newItem.appendChild( newDoc.createTextNode( value.strip() ) )
            node.appendChild( newItem )
        
def multiSplit( string, split ):
    ret = []
    start = 0
    for pos in range( 0, len( string ) ):
        if string[pos] in split:
            ret.append( string[start:pos] )
            start = pos + 1
    ret.append( stripQuotes( string[start:] ) )
    return ret
        
def replaceParameter( value ):
    ignore = [] 
    while not value.count( '$' ) == len( ignore ):
        if value.find( '${' ) > -1:
            start = value.find( '${' )
            stop = value.find( '}' )
            parameter = value[ start : stop + 1 ]
            if not parameter[2:-1] in ignore:
                if environ.has_key( parameter[2:-1] ):
                    value = value.replace( parameter, stripQuotes( environ[parameter[2:-1]] ) )
                else:
                    ignore.append( parameter[2:-1] )
        elif value.find( '$' ) > -1:
            tmp = value[value.find( '$' ):].strip()
            parameter = multiSplit( tmp, '/: ' )[0]
            if not parameter[1:] in ignore:
                if environ.has_key( parameter[1:] ):
                    value = value.replace( parameter, stripQuotes( environ[parameter[1:]] ) )
                else:
                    ignore.append( parameter[1:] )
    return value
    
def updateParamNode( node, value, yaimParam, description = '' ):
#    if isArray(value) == isGliteArray(node):
        value = replaceParameter( value )
        if isGliteScalar( node ):
            valueNodes = node.getElementsByTagName( 'value' )
            if len( valueNodes ) == 0:
                node.setAttribute( 'value', value )
            else:
                updateArrayParameter( node, "'%s'" % value, yaimParam, description )
        else:
            updateArrayParameter( node, value, yaimParam, description )
        if description == '':
            node.setAttribute( 'description', 'Parameter defined in %s by YAIM parameter %s'\
                       % ( yaim_file, yaimParam ) )
        else:
            node.setAttribute( 'description', description )

def mapYaim2gLite( param ):
    if map.has_key( param ):
        ret = map[param]
    else:
        ret = [param]
    return ret    

def createContainer( tag, name ):
    container = "<%s name='%s'><parameters/></%s>" \
        %( tag.lower(), name.lower(), tag.lower() )
    node = parseString( container )
    return node.documentElement

def createParameter( voName, value, yaimParam ):
    for param in mapYaim2gLite( yaimParam ):
        paramNode=parseString( "<%s/>" % param ).documentElement
        updateParamNode( paramNode, value, yaimParam )
        vo[voName].getElementsByTagName( 'parameters' )[0].appendChild( paramNode )
        
def stripQuotes( param ):
    return param.strip( '"' ).strip( "'" ).strip()

def getPoolName( vo ):
    user = ( '', '' )
    users = open( replaceParameter( environ['USERS_CONF'] ) ).readlines()
    for line in users:
        line = line.strip()
        if not line == '' and not line[0] == '#':
            if line.split( ':' )[4].lower() == vo.lower():
                #    user = ( <first pool account name>, <pool group>)
                user = ( line.split( ':' )[1], line.split( ':' )[3] )
                break
    return user
    
def updateContainerParameter( yaimName, value ):
    ( tag, tagName, param ) = yaimName.split( '_', 2 )
    tagName = tagName.lower()
    #    ToDo: Put case insensitive check on the VO name
    if tagName in stripQuotes( environ['VOS'].lower() ).split():
        if not vo.has_key( tagName ):
            vo[tagName] = xmlUtils.getContainerTemplate( 'vo-template' )
            vo[tagName].setAttribute( 'name', tagName )
            # create default VO parameter structure
            # default: pool account creation disabled
            ( name, group ) = getPoolName ( tagName )
            base = name.rstrip( '0123456789' )
            createParameter( tagName, base, 'pool.account.basename' )
            createParameter( tagName, group, 'pool.account.group' )
            createParameter( tagName, tagName.lower(), 'vo.name' )
        if param == 'VOMSES':
            if value.strip() != '':
                updateParamNode( vo[tagName].getElementsByTagName( 'vo.name' )[0], \
                                  stripQuotes( value.split( ' ' )[0] ), 'vo.name' )
                updateParamNode( vo[tagName].getElementsByTagName( 'voms.hostname' )[0], \
                                  value.split( ' ' )[1], 'voms.hostname' )
                updateParamNode( vo[tagName].getElementsByTagName( 'voms.port.number' )[0], \
                                  value.split( ' ' )[2], 'voms.port.number' )
                updateParamNode( vo[tagName].getElementsByTagName( 'voms.cert.subj' )[0], \
                                  stripQuotes( value.split( ' ' )[3] ), 'voms.cert.subj' )

        else:
            for gParam in mapYaim2gLite ( param ):
                nodes = vo[tagName].getElementsByTagName( gParam )
                if len( nodes ) == 0:
                    createParameter( tagName, value, gParam )
                else:
                    updateParamNode( nodes[0], value, yaimName )

#    glite UI set structure
def createUIsetContainer( VO ):
    setContainer = GliteUIsetParams( VO )
    if environ.has_key( 'WMS_HOST' ):
        setContainer.addNS( replaceParameter( environ['WMS_HOST'] ), [replaceParameter( environ['WMS_HOST'] )] )	
    else:
        print "[WARNING] No WMS_HOST defined. gLite UI will not be configured !!"
    setContainer.addParameter( "ui.voms.server", getParam( vo[VO], 'voms.hostname' ) )
    setContainer.addParameter( "ui.voms.port", getParam( vo[VO], 'voms.port.number' ) )
    setContainer.addParameter( "ui.voms.cert.url", "" )
    setContainer.addParameter( "ui.voms.cert.subject", getParam( vo[VO], 'voms.cert.subj' ) )
    setContainer.addParameter( "ui.MyProxyServer", replaceParameter( environ['PX_HOST'] ) )
    setContainer.addParameter( "ui.HLRLocation", "" )
    setContainer.addArrayParameter( "ui.wms-proxy.endpoints", ['https://%s:7443/glite_wms_wmproxy_server' % replaceParameter( environ['WMS_HOST'] )] )
    return setContainer.getNode()
    # vo, vo, ns.name, lb.name, voms.name, voms.port, voms.cert.subj, wmsproxy.endpoints, myproxy.name, hlr.location

def updateTorqueConfig( dom ):
    print "Parsing torque configuration"
    instances = dom.getElementsByTagName( 'instance' )
    yaimQueueList = replaceParameter( environ['QUEUES'] ).split( " " )
    yaimWNList = open( replaceParameter( environ['WN_LIST'] ) ).readlines()
    # Cleanup wn entries from whitespace
    tmpList = []
    for wn in yaimWNList:
        if not wn.strip() == '':
            tmpList.append( wn.strip() )
    yaimWNList = tmpList

    rootNode = dom.getElementsByTagName( 'config' )[0]
    print "Worker nodes:"
    for wn in dom.getElementsByTagName( 'torque-wn.name' ):
        if wn.getAttribute( 'value' ) in yaimWNList:
            print "Keeping configuration for WN " + wn.getAttribute( 'value' )
            yaimWNList.remove( wn.getAttribute( 'value' ) )
        else:
            print "Remove configuration for WN " + wn.getAttribute( 'value' )
            instanceNode = wn.parentNode.parentNode
            rootNode.removeChild( instanceNode )
    for wn in yaimWNList:
        print "Adding configuration for WN " + wn
        rootNode.appendChild( createWNInstance( wn ) )

    print "Queues:"
    queue_list = {}
    for queue in dom.getElementsByTagName( 'queue.name' ):
        if queue.getAttribute( 'value' ) in yaimQueueList:
            print "Keeping configuration for queue " + queue.getAttribute( 'value' )
            yaimQueueList.remove( queue.getAttribute( 'value' ) )
            queue_list[queue.getAttribute( 'value' )] = queue.parentNode   
        else:
            print "Remove configuration for queue " + queue.getAttribute( 'value' )
            instanceNode = queue.parentNode.parentNode
            rootNode.removeChild( instanceNode )

    for queue in yaimQueueList:
        print "Adding configuration for queue " + queue
        queue_node = createQueueInstance( queue )
        queue_list[queue] = queue_node  
        rootNode.appendChild( queue_node )

    #    Set up the queue group ACLs
    acl = {}
    for param in environ:
        if param.startswith( "VO_" ) and param.endswith( "_QUEUES" ):
            vo_name = param.split( "_" )[1]
            group = getPoolName ( vo_name )[1]
            queue_names = environ[param].split( ' ' )
            acl_str = " +%s" % group
            for queue_name in queue_names:
                #print "ACL for: %s" % queue_name
                if acl.has_key( queue_name ):
                    acl[queue_name] = acl[queue_name] + acl_str
                else:
                    acl[queue_name] = acl_str
    
    for queue in queue_list:
        updateParamNode( queue_list[queue].getElementsByTagName( 'queue.acl.groups' )[0], acl[queue], "", "Value derived from VO definition" )
                    
#    glite torque server WN structure
def createWNInstance( WN ):
    container = xmlUtils.getContainerTemplate( 'glite-wn' )
    container.setAttribute( 'name', WN )
    updateParamNode( container.getElementsByTagName( 'torque-wn.name' )[0], WN, "", "from WN-LIST file" )
    return container
   
def createQueueInstance( queue ):
    container = xmlUtils.getContainerTemplate( 'torque-queue' )
    container.setAttribute( 'name', 'torque-queue-%s' % queue )
    updateParamNode( container.getElementsByTagName( 'queue.name' )[0], queue, 'QUEUES' )
    return container
        
def getParam( dom, name ):
    return dom.getElementsByTagName( name )[0].getAttribute( 'value' )
    
def buildUIsets( ui_conf ):
    ui_dom, ui_keys = ui_conf
    sets = ui_dom.getElementsByTagName( 'set' )
    #    remove all sets
    for set in sets:
        ui_dom.documentElement.removeChild( set )
        
    #    for each VO create the set
    for VO in vo:
        ui_dom.documentElement.appendChild( createUIsetContainer( VO ) )

def parse_yaim( yaim_file ):
    try:
        lines=open( yaim_file, 'r' ).readlines()    
    except IOError:
        print "Error: Site Info file %s not found" % yaim_file
        sys.exit( 1 )
    lastkey=''
    for line in lines:
        line=line.replace( '\"', '' ).replace( '\'', '' )
        if line.find( '#' ) > -1:
            line=line[0:line.find( '#' )]
        if line.find( '=' ) > -1:
            ( key, value ) = line.split( '=', 1 )
            environ[key]=value[:-1].strip()
            lastkey=key
        else:
            if len( line.strip() ) > 1 and len( lastkey ) > 1:
                environ[key]=environ[key] + " " + line[:-1].strip()
    return 

def parseVomsesString( string ):
    split = [[' ','"',"'"],['"',"'"]]
    if string.count('"') + string.count("'") == 0:
        return string.split(' ')
    ret = []
    tmp = string.split(" ", 3)
    for i in xrange(3):
        ret.append(stripQuotes(tmp[i]))
    tmp[3] = tmp[3].lstrip()
    if not tmp[3][0] in split[1]:
        end = tmp[3].find(' ')
    else:
        end = tmp[3][1:].find(tmp[3][0])
    ret.append(stripQuotes(tmp[3][:end]))
    tmp[3] = stripQuotes(tmp[3][end+1:]) + ' '
    ret.append(stripQuotes(tmp[3][:tmp[3].find(' ')]))

    return ret

def getParamNames( node ):
    nodeList = []
    parameterNodes = node.getElementsByTagName( 'parameters' )
    for node in parameterNodes:
        for child in node.childNodes:
            if child.nodeType == Node.ELEMENT_NODE:
                nodeList.append( child.nodeName )
    return nodeList

def getMappingAndDefaults( map_file ):
    try:
        lines=open( map_file, 'r' ).readlines()
    except IOError:
        print "Error: Map file %s not found" % map_file
        sys.exit( 1 )
        
    for line in lines:
        line.strip()
        if line[0]!="#" and len( line ) > 1:
            if line.count( '=' ) > 0:
                ( key, value ) = line.split( '=', 1 )
                defaults[key] = value.strip()
            if line.count( ':' ) > 0:
                ( key, value ) = line.split( ':', 1 )
                map[key] = value.strip().split( ',' )
    return 

def parseNewVOParams( ):
    for vo in replaceParameter(environ['VOS'].split()):
        tmpParam = {}
        voParamsFile = "%s/vo.d/%s" % (environ['config_dir'], vo.lower())
        if os.path.exists(voParamsFile):
            envLines = os.popen("%s/../libexec/parse_bash_params %s" % (os.environ['FUNCTIONS_DIR'], voParamsFile)).readlines()
            configLines = open(voParamsFile).readlines()
            for param in envLines:
                if not (param.strip() == '' or param.strip() == '#' or param.find('=') == -1 ):
                    (p_name, p_value) = param.split("=",1)
                    tmpParam[p_name] = p_value
            for line in configLines:
                p_name = line.split("=",1)[0]
                if tmpParam.has_key(p_name):
                    #    Add parameter entry following old rules. 
                    #    Shell forbidden characters will not cause problems since these are only Python dictionary object
                    environ["VO_%s_%s" %(vo.upper(), p_name.upper())] = tmpParam[p_name]
                    #print "Added VO_%s_%s = %s" % (vo.upper(), p_name.upper(), tmpParam[p_name])
    return 

def checkArrayValues(node, value):
    ret = False
    for child in node.childNodes:
        if child.nodeType == Node.ELEMENT_NODE and child.nodeName == 'value':
          if child.hasChildNodes():
            if child.childNodes[0].nodeValue == value:
                ret = True
          else:
            child.parentNode.removeChild(child)
    return ret
 
def addArrayEntry(node, value):
    #print value
    if not checkArrayValues(node, value):
        newDoc = parseString( '<value/>' )
        newItem = newDoc.documentElement
        if not value.strip() == '':
            newItem.appendChild( newDoc.createTextNode( value.strip() ) )
        node.appendChild( newItem )


if __name__ == '__main__':

    # Check that the required environment has been defined
    for variable in ['FUNCTIONS_DIR', 'SITE_INFO', 'NODE_TYPE_LIST' ]:
        if not os.environ.has_key( variable ):
            print "Error: %s not set." %variable
            sys.exit( 1 )
    vo={}
    map={}
    defaults={}
    environ={}
        
    # Get Mapping and default parameters
    map_file=replaceParameter(os.environ['FUNCTIONS_DIR']) + '/../libexec/gLite.def'
    getMappingAndDefaults( map_file ) 

    # Parse YAIM parameters
    yaim_file=os.environ['SITE_INFO'] 
    environ = os.environ

    #    Add New VO parameters
    parseNewVOParams()

    # Merge defaults with parsed YAIM parameters
    environ.update( defaults )

    working_dir = replaceParameter( environ['GLITE_LOCATION'] ) + '/etc/config/'
    
    # Copy files from templates if they doesn't exist in the config dir
    templates = []
    for file in os.listdir( working_dir + 'templates' ):
        if file.endswith( '.cfg.xml' ):
            templates.append( file )

    for file in templates:
        try:
            os.stat( working_dir + file )
        except OSError:
            shutil.copy( working_dir + '/templates/%s' % file, working_dir )
            
    # Temporary hack        
    try:
        os.remove( working_dir + 'glite-rgma-servicetool-externalServices.cfg.xml' )
        os.remove( working_dir + 'vo-list.cfg.xml' )
        os.remove( working_dir + 'glite-service-discovery.file-based-example.cfg.xml' )
        os.remove( working_dir + 'glite-ce-site-config.xml' )
        os.remove( working_dir + 'glite-lfc-client.cfg.xml' )
    except:
        pass

    # Parse all gLite configuration files
    gLiteDom = {}
    for file in templates:
        try:
            tmpDom = parse( working_dir + file )
        except IOError:
            print "Skipping file %s" % file
            continue
            
        # Read VO definitions from the glite-global.cfg.xml file if exist
        if file == 'glite-global.cfg.xml':
            for VO in tmpDom.getElementsByTagName( 'vo' ):
                vo[VO.getAttribute( 'name' ).lower()] = VO
                tmpDom.documentElement.removeChild( VO )
        # Update parameters if they are in templates but not in the active config file
        try:
            templateDom = parse(working_dir + "templates/" + file)
            templateParameterList = getParamNames(templateDom)
        except IOError:
            print "WARNING file %s has no template" % file
        parameterList = getParamNames( tmpDom )
        for parameter in templateParameterList:
            if not parameter in parameterList:
                print "Adding parameter %s into %s" % (parameter, file)
                tmpDom.getElementsByTagName( 'parameters' )[0].\
                    appendChild (templateDom.getElementsByTagName( parameter )[0])
                parameterList.append( parameter )                
        gLiteDom[file] = ( tmpDom, parameterList )        
    
    #    Special treatement for docndor TCP port range
    if environ.has_key( "GLOBUS_TCP_PORT_RANGE" ):
        environ['condor.LOWPORT'] =\
            multiSplit(stripQuotes( environ['GLOBUS_TCP_PORT_RANGE'] ), " ,")[0] 
        environ['condor.HIGHPORT'] =\
            multiSplit(stripQuotes( environ['GLOBUS_TCP_PORT_RANGE'] ), ' ,' )[1]
            
    for param in environ:
        #print param
        if param.startswith( "VO_" ) and not param in ['VO_SW_DIR']:
            # Create/modify VO definition
            updateContainerParameter( param, environ[param] ) 
        elif param.find("_GROUP_ENABLE") > -1:
            pass
            #for entry in environ[param].split(" "):
                # add array entry ......
             #   addArrayEntry(vo[entry.lower()].getElementsByTagName( 'cemon.queues' )[0], \
              #                stripQuotes( param.split("_")[0].lower()))
                          
                
                #updateParamNode( vo[entry.lower()].getElementsByTagName( 'cemon.queues' )[0], \
                #                  stripQuotes( param.split("_")[0].lower()), 'cemon.queues' )
        else:
            # Update parameter in all configuration files if exist
            gLiteParamList = mapYaim2gLite( param )
            for gLiteParam in gLiteParamList:
                for gDOM in gLiteDom:
                    if gLiteParam in gLiteDom[gDOM][1]:
                        nodes = gLiteDom[gDOM][0].getElementsByTagName( gLiteParam )
                        for node in nodes:
                            updateParamNode( node, environ[param], param )

    #    Manage QUEUES
    import string
    for queue in os.environ['QUEUES'].split(" "):
        queue_ = queue.translate(string.maketrans('.-','__')).upper()
        for entry in environ["%s_GROUP_ENABLE" % queue_ ].split(" "):
            try:
                # add array entry ......
                addArrayEntry(vo[entry.lower()].getElementsByTagName( 'cemon.queues' )[0], \
                              queue.lower())
            except:
                #    Should be possible to ignore it since this information is used only in TORQUE_server (not Python)
                #    And publication on glite-CE (done through config_gip)
                pass
            
    # Check if we configure also UI
    if 'UI' in os.environ['NODE_TYPE_LIST'].split() or \
       'TAR_UI' in os.environ['NODE_TYPE_LIST'].split() or \
       'VOBOX' in os.environ['NODE_TYPE_LIST'].split():
            buildUIsets( gLiteDom['glite-ui.cfg.xml'] )

    #Write out new files and back up the old ones
    for file in templates:
        if file == 'glite-global.cfg.xml':
            for voCont in vo:
                gLiteDom[file][0].documentElement.appendChild( vo[voCont].cloneNode( 'deep' ) ) 
        try:
            os.rename( working_dir + file, working_dir + file + '.bak' )
        except OSError:
            continue
        output = open( working_dir + file, 'w' )
        gliteXMLWriter().write( gLiteDom[file][0], output )
        output.close()
        
    sys.exit( 0 )
